class Machine {
	public void start() {

		System.out.println("Machine started");

	}

}

		// robi override metody machine
class Camera extends Machine {

	public void start() {

		System.out.println("Camera started");

	}

	public void snap() {

		System.out.println("Photo taken");
	}

}

public class App {

	public static void main(String[] args) {

		// mozna zmienic variables ktore odnosza sie do maszyny ktora zawsze
		// bedzie maszyna
		Machine machine1 = new Machine();
		Camera camera1 = new Camera();

		machine1.start();
		camera1.start();
		camera1.snap();

		//////// Upcasting /////////
		// upcasting jest safe bo polimorfizm go gwarantuje
		// Machine machine2 = new Camera();

		// I've upcasted this camera1 to machine2
		// poszedlem z camera klase wyzej do maszyny
		Machine machine2 = camera1;
		// camera sie uruchomila
		machine2.start();
		
		
		// to juz nie zadziala
		// odnosi sie do camera ale snap nie dziala
		//trzeba to jakos ominac
		// machine2.snap();
		
		
		
		////////Downcasting//////////
		Machine machine3 = new Camera();
		// przy downcasting java chce potwierdzenia ze wiesz co robisz
		// dlatego trzeba dac w nawiasie (Camera)
		// downacting mozna namieszac dlatego potwierdzenie
		Camera camera2 = (Camera)machine3;
		camera2.start();
		camera2.snap();
		
		// przyklad jak namieszac
		// runtime error
		// Machine machine4 = new Machine();
		// Camera camera3 = (Camera)machine4;
		// camera3.start();
		// camera3.snap();
		
		
	}

}
